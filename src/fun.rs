#![allow(unused)]

use std::fs::{OpenOptions, File};
use std::io::{self, prelude::*, BufRead, BufReader, Write, Result};
use std::path::Path;
use std::env;
use colored::*;

// Return the path of the .todolist file.
pub fn path() -> String{
    let home = env::var("HOME").unwrap();
    let path = format!("{}/.todolist", &home);

    return path;
}

// Check if the .todolist file exist.
pub fn file() {
    let path = path();

    if Path::new(&path).is_file() {
        println!("");
    } else {
        let mut _file = File::create(path);
    }
}

// Help 
pub fn help() {
    println!("
    Todolist is a small program that I wrote to improve my 
    skills in Rust. Its made for set task to do and list it.

    Usage : todolist [cmd] [task]

    - todolist add [task]
        Add a task to do.

    - todolist rm [task index]
        Remove a task by locating it with is index.
    
    - todolist done [task index]
        Set a task as done by locating it with is index.

    - todolist list 
        Lists all tasks
    ");
}

// Add a thing to do in the todolist.
pub fn add(mut todo: &String) -> Result<()> {
    let path: String = path();
    let task: String = format!("[ ] {}\n", todo);


    let mut todolist = OpenOptions::new()
        .append(true)
        .open(path)?;

        todolist.write_all(task.as_bytes());

        Ok(())
}

// Print tasks.
pub fn tasks() -> io::Result<()> {
    let path: String = path();

    let file = File::open(path)?;

    let reader = BufReader::new(file);

    let mut i = 1;

    for line in reader.lines() {
        let mut task: String = line?.to_string();

        let symbol = (&task[..4]).to_string();

        task = (&task[4..]).to_string();

        if symbol == "[*] " {
            task = format!("{}", task.strikethrough());
        }

        println!("{} {}", i, task);
        
        i = i + 1;
    }

    Ok(())
}

pub fn done(tasks: Vec<String>) -> io::Result<(), std::io::Error> {
    let path: String = path();

    let mut i_tasks: Vec<i32> = vec![];
    for task in tasks {
        let n: i32 = task.parse().unwrap();
        i_tasks.push(n);
    }

    

}

