#![allow(unused)]

use std::env;

pub mod fun;

fn main() {
    let mut args: Vec<String> = env::args().collect();
    args.remove(0);

    let add: String = String::from("add");  

    fun::file();
    
    let arg: &String = &args[0];
    let cmd: [String; 4] = [
        String::from("help"),
        String::from("add"),
        String::from("tasks"),
        String::from("done"),
    ];

    let help: String = "help".to_string();
    if args[0] == cmd[0] {
        fun::help();
    } else if args[0] == cmd[1] {
        let task: &String = &args[1];
        fun::add(task);
    } else if args[0] == cmd[2] {
        fun::tasks();
    } else if args[0] == cmd[3] {
        args.remove(0);
        fun::done(args);
    }
    
}